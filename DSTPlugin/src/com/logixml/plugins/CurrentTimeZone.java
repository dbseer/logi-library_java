package com.logixml.plugins;

public class CurrentTimeZone {
	
    public String Status = "";
    
    public String Message = "";

    public String CountryCode = "";

    public String ZoneName = "";

    public String Abbreviation = "";

    public String GmtOffset  = "";

    public String Dst  = "";

    public String Timestamp = "";

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getCountryCode() {
		return CountryCode;
	}

	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}

	public String getZoneName() {
		return ZoneName;
	}

	public void setZoneName(String zoneName) {
		ZoneName = zoneName;
	}

	public String getAbbreviation() {
		return Abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		Abbreviation = abbreviation;
	}

	public String getGmtOffset() {
		return GmtOffset;
	}

	public void setGmtOffset(String gmtOffset) {
		GmtOffset = gmtOffset;
	}

	public String getDst() {
		return Dst;
	}

	public void setDst(String dst) {
		Dst = dst;
	}

	public String getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}
    
}
