package com.logixml.plugins;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class plugin
{
	private static final String USE_API = "USE_API";

	public void GetGlobalTimeZone(LogiPluginObjects10 rdObjects)
	   {
			Hashtable<?, ?> key = rdObjects.getPluginParameters();
		   	TimezoneDB timeZone = new TimezoneDB();
		   	if (null != key.get("APIKey"))
		   		timeZone.setKey(key.get("APIKey").toString());
		   
		   	Map<String, String> timeZones = new HashMap<String, String>();
	
	      try
	      {
	    	 Document objDoc = rdObjects.getCurrentData();
	         NodeList nl = objDoc.getElementsByTagName("procUtc");
	         
       	 DOMSource domSource = new DOMSource(objDoc);
       	 StringWriter writer = new StringWriter();
       	 StreamResult result = new StreamResult(writer);
       	 TransformerFactory tf = TransformerFactory.newInstance();
       	 Transformer transformer = tf.newTransformer();
       	 transformer.transform(domSource, result);
       	 System.out.println("XML IN String format is: \n" + writer.toString());
	         
	         
	         if (nl.getLength() == 0)
	         {
	            throw new Exception("The report is missing the DataLayer element.");
	         }
	         
	         for (int i = 0; i < nl.getLength(); i++) {
	        	 Element objNode = (Element) nl.item(i);
	        	 String name = objNode.getAttribute("TIMEZONE");
	        	 CurrentTimeZone ctz = new CurrentTimeZone();
	        	 int offset = 0;
	        	 
	        	 if(USE_API.equals(objNode.getAttribute("CUR_OFFSET"))){
	        		 if(!timeZones.containsKey(name)){
		        		 if(timeZone.getKey().isEmpty())
		        			 offset = Integer.parseInt(objNode.getAttribute("OFFSET"));
		        		 else{
		        			 ctz = timeZone.getCurrentTimezoneId(name);
		        			 offset = (Integer.parseInt(ctz.getGmtOffset()) / 60);
		        		 }
		        		 timeZones.put(name, String.valueOf(offset));    
	        		 }
		        	 else{
			        	 offset = (Integer.parseInt(timeZones.get(name))); 
			         }
	        	 }
	        	 else {
	        		 offset = Integer.parseInt(objNode.getAttribute("CUR_OFFSET"));
	        	 }
    	 
	        	 //offset = 180;
	        	 
	        	int currentOffset = Integer.parseInt(objNode.getAttribute("OFFSET"));
                if (currentOffset != offset)
                {
	            	DateFormat origialFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
	                Date localTime = origialFormat.parse(objNode.getAttribute("LOCTIME"));
	                
	                //update utc time:
	                Calendar cal = Calendar.getInstance();
	                cal.setTime(localTime);
	                cal.add(Calendar.MINUTE, -1 * offset);
	                
	                DateFormat timeFormat = new SimpleDateFormat("kk:mm", Locale.ENGLISH);
	                String newTime = timeFormat.format(cal.getTime());
	                
	                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	                String newDate = dateFormat.format(cal.getTime());
	                String updatedDate = newDate + " " + newTime;
	                
	                
	                                       
	                String scheduleXml = editScheduleXml(objNode.getAttribute("ScheduleXml"), newDate, newTime);
	               
	                objNode.setAttribute("UTCTIME",updatedDate);
	                objNode.setAttribute("OFFSET", String.valueOf(offset));
	                objNode.setAttribute("ScheduleXml", scheduleXml);
	                
	                
	              //update next run time:
	                
	                if(objNode.getAttribute("NEXTRUN") != ""){
	                Date nextRun = origialFormat.parse(objNode.getAttribute("NEXTRUN"));
	                
	                cal.setTime(nextRun);
	                cal.add(Calendar.MINUTE, (currentOffset - offset));
	                String newNextTime = timeFormat.format(cal.getTime());
	                String newNectDate = dateFormat.format(cal.getTime());
	                String updatedTimeNextRun = newNectDate + " " + newNextTime;
	                objNode.setAttribute("NEXTRUN",updatedTimeNextRun);
	                }
	                else {objNode.setAttribute("NEXTRUN",null);
	                }
	                
	              //update last run time:
	                if(objNode.getAttribute("LASTRUN") != ""){
	                Date lastRun = origialFormat.parse(objNode.getAttribute("LASTRUN"));
	                
	                cal.setTime(lastRun);
	                cal.add(Calendar.MINUTE, (currentOffset - offset));
	                String newLastTime = timeFormat.format(cal.getTime());
	                String newLastDate = dateFormat.format(cal.getTime());
	                String updatedTimeLastRun = newLastDate + " " + newLastTime;
	                objNode.setAttribute("LASTRUN",updatedTimeLastRun);
	                }
	                else {objNode.setAttribute("LASTRUN",null);
	                }
	                
	                objNode.setAttribute("UPDATERECORD","1");
	             }
	         }
	   }
	   catch (Exception ex)
	   {
	      ex.printStackTrace();
	      System.out.println("SetCustomerQuery Error " + ex.getMessage());
	   }
	}
 
   public void setUTCTimeZone(LogiPluginObjects10 rdObjects)
   {	   
      try
      {
    	 Document objDoc = rdObjects.getCurrentData();
         NodeList nl = objDoc.getElementsByTagName("procUtcWhenSave");
         
         for (int i = 0; i < nl.getLength(); i++) {
        	 Element objNode = (Element) nl.item(i);
             String utcTime = objNode.getAttribute("UTCTIME");
             String utcDate = utcTime.split(" ")[0];
                utcTime = utcTime.split(" ")[1];
             String scheduleXml = editScheduleXml(objNode.getAttribute("ScheduleXml"),utcDate, utcTime);
             objNode.setAttribute("ScheduleXml", scheduleXml);
         }    
	   }
	   catch (Exception ex)
	   {
	      ex.printStackTrace();
	      System.out.println("SetCustomerQuery Error " + ex.getMessage());
	   }
   }
   
   private String editScheduleXml(String scheduleXml, String startDate, String startTime) throws JDOMException, IOException{	   
	   
	   org.jdom2.Document doc = (org.jdom2.Document) new SAXBuilder().build(new InputSource(new ByteArrayInputStream(scheduleXml.getBytes("utf-8"))));
		org.jdom2.Element rootNode = doc.getRootElement();

		rootNode.setAttribute("FirstRunTime",startTime);
		rootNode.setAttribute("StartDate",startDate);
		XMLOutputter xmlOutput = new XMLOutputter();
		xmlOutput.setFormat(Format.getPrettyFormat());
		StringWriter sw = new StringWriter();
		xmlOutput.output(doc.getContent(), sw);
	    StringBuffer sb = sw.getBuffer();
	    
	  
	   return sb.toString();
   }
   
}