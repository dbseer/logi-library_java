package com.logixml.plugins;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Fetch timezone ID from timezonedb.com and implement a simple cache to reduce 
 * request load.
 * 
 * @author joe
 *
 */
public class TimezoneDB {
	
	private String timezoneDbKey="";
	
	public void setKey(String key) {
		this.timezoneDbKey = key;
	}

	public String getKey( ) {
		return this.timezoneDbKey;
	}
	
	public CurrentTimeZone getCurrentTimezoneId(String zone) {
		CurrentTimeZone ctz = new CurrentTimeZone();
		 StringBuffer buf = new StringBuffer();
		 try {
			 
			URL url = new URL("http://api.timezonedb.com/?zone=" + zone + "&key=" + timezoneDbKey);
			
			URLConnection con = url.openConnection();
			con.setConnectTimeout(5000); // 5s
			con.setReadTimeout(5000);
			InputStream in = con.getInputStream();
			
		 	BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		 	
		    String line;
		    while ((line = reader.readLine()) != null) {
		        buf.append(line);
		    }
		    reader.close();
      
			ctz.setZoneName(getZone("zoneName",buf));	
			ctz.setStatus(getZone("status",buf));	
			ctz.setMessage(getZone("message",buf));	
			ctz.setCountryCode(getZone("countryCode",buf));	
			ctz.setAbbreviation(getZone("abbreviation",buf));	
			ctz.setGmtOffset(getZone("gmtOffset",buf));
			ctz.setDst(getZone("dst",buf));	
			ctz.setTimestamp(getZone("timestamp",buf));		
				
		    return ctz;
		    
		} catch (MalformedURLException e) {
	    // ...
		} catch (IOException e) {
			e.printStackTrace();
		}
		 
		 return null;
	}	
	
	public String getZone(String name, StringBuffer buf){
	    int start = buf.indexOf("<" + name + ">",0) + ("<" + name + ">").length();
	    int end = buf.indexOf("</" + name + ">",0);
	 
	    name = buf.substring(start,end);	
		return name;
	}
	
}
