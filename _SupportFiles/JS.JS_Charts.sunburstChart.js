drawSunburstChart('sunburstContainer', seqJson);

function drawSunburstChart(chartContainerID, data) {
	// Dimensions of sunburst.

	if(d3.select("#sunburstChart").empty() && d3.select("#sequence").empty()){
		$("#"+chartContainerID).prepend("<div id='sunburstChart' class='' style='width:75%; margin:auto !important;'>\
								<div id='explanation' class='margin' style='visibility: hidden;'>\
									<span id='percentage'></span>\
									<br>of total delays is : \
									<span id='desc'></span>\
								</div>\
							</div>");
	}else if(d3.select("#sunburstChart").empty() && !d3.select("#sequence").empty()){
		$("#sequence").after("<div id='sunburstChart' class='' style='width:75%; margin:auto !important;'>\
								<div id='explanation' class='margin' style='visibility: hidden;'>\
									<span id='percentage'></span>\
									<br>of total delays is : \
									<span id='desc'></span>\
								</div>\
							</div>");
	}
	var CContainer = $('#sunburstChart');
	var width = CContainer.width();
	var height = CContainer.width();
	var radius = Math.min(width, height) / 2.5;

	// Breadcrumb dimensions: width, height, spacing, width of tip/tail.
	var b = {
		w: 125,
		h: 30,
		s: 3,
		t: 10
	};
	var delaysColors = {
		"Carrier Delay": "rgb(231, 150, 156)",
		"NAS Delay": "rgb(123, 65, 115)",
		"LateAircraft Delay": "#90A4AE",
		"Weather Delay": "rgb(222, 158, 214)",
		"Security Delay": "rgb(132, 60, 57)"
	};

	var delaysDesc = {
		"Carrier Delay": "The cause of the cancellation or delay was due to circumstances within the airline's control (e.g. maintenance or crew problems, aircraft cleaning, baggage loading, fueling, etc.).",
		"Weather Delay": "Significant meteorological conditions (actual or forecasted) that, in the judgment of the carrier, delays or prevents the operation of a flight such as tornado, blizzard or hurricane.",
		"NAS Delay": "Delays and cancellations attributable to the national aviation system that refer to a broad set of conditions, such as non-extreme weather conditions, airport operations, heavy traffic volume, and air traffic control.",
		"LateAircraft Delay": "A previous flight with same aircraft arrived late, causing the present flight to depart late.",
		"Security Delay": "Delays or cancellations caused by evacuation of a terminal or concourse, re-boarding of aircraft because of security breach, inoperative screening equipment and/or long lines in excess of 29 minutes at screening areas."
	};

	var fill = d3.scale.category20b();

	// Total size of all segments; we set this later, after loading the data.
	var totalSize = 0;

	var vis = d3.select("#" + chartContainerID + " div#sunburstChart").append("svg:svg")
		.attr({
			"width": '100%',
			"height": '100%'
		})
		.attr('viewBox', '0 0 ' + width * 1.3 + ' ' + (width + 40))
		.attr('preserveAspectRatio', 'xMinYMin')
		.append("svg:g")
		.attr("id", "burstContainer")
		.attr("transform", "translate(" + width / 1.55 + "," + height / 2 + ")");

	var partition = d3.layout.partition()
		.size([2 * Math.PI, radius * radius])
		.value(function (d) {
			return d.size;
		});

	var arc = d3.svg.arc()
		.startAngle(function (d) {
			return d.x;
		})
		.endAngle(function (d) {
			return d.x + d.dx;
		})
		.innerRadius(function (d) {
			return Math.sqrt(d.y);
		})
		.outerRadius(function (d) {
			if (d.depth == 2) {
				return Math.sqrt(d.y + d.dy) * 1.25;
			} else {
				return Math.sqrt(d.y + d.dy);
			}
		});

	// Getting the data.
	var fullNameMap = {};
	fullNameJson.forEach(function (airport) {
		var name = airport.airports;
		fullNameMap[name] = airport.fullname;
	});
	var csv = data;
	var json = buildHierarchy(csv);
	createVisualization(json);

	// Main function to draw and set up the visualization, once we have the data.
	function createVisualization(json) {

		// Basic setup of page elements.
		initializeBreadcrumbTrail();
		drawLegend();
		d3.select("#togglelegend").on("click", toggleLegend);

		// Bounding circle underneath the sunburst, to make it easier to detect
		// when the mouse leaves the parent g.
		vis.append("svg:circle")
		.attr("r", radius)
		.style("opacity", 0);

		// For efficiency, filter nodes to keep only those large enough to see.
		var nodes = partition.nodes(json)
			.filter(function (d) {
				return (d.dx > 0.005); // 0.005 radians = 0.29 degrees
			});
		var group = vis.data([json]).selectAll("group.group")
			.data(nodes)
			.enter().append("svg:g")
			.on("mouseover", mouseoverFun);

		var path = group.append("svg:path")
			.attr("display", function (d) {
				return d.depth ? null : "none";
			})
			.attr("d", arc)
			.attr("id", "burstPath")
			.attr("fill-rule", "evenodd")
			.style("fill", function (d, i) {
				if (d.depth == 2) {
					return delaysColors[d.name]
				} else {
					return fill(d.name)
				}
			})
			.style("opacity", 1);

		group.append("text")
		.each(function (d) {
			d.angle = (d.x + (d.x + d.dx)) / 2;
		})
		.attr("dy", ".35em")
		.style("font-family", "helvetica, arial, sans-serif")
		.style("font-size", "14px")
		.style("font-weight", "bold")
		.attr("text-anchor", function (d) {
			return d.angle > Math.PI ? "end" : null;
		})
		.attr("transform", function (d) {
			return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
			 + "translate(" + (Math.sqrt(d.y) + 5) + ")"
			 + (d.angle > Math.PI ? "rotate(180)" : "");
		})
		.text(function (d) {
			if (d.depth == 2) {
				var name = d.name.substr(0, d.name.length - 5);
				return name;
			} else if (d.depth == 1) {
				return d.name;
			} else {
				return null;
			}
		})
		.attr("class", "key tooltips white")
		.attr("data-original-title", function (d) {
			return fullNameMap[d.name]
		})
		.attr("data-container", "body");

		// Add the mouseleave handler to the bounding circle.
		d3.select("#burstContainer").on("mouseleave", mouseleaveFun);

		// Get total size of the tree = value of root node from partition.
		totalSize = path.node().__data__.value;

		$(document).ready(function () {
			var element = path[0][0].__data__.children[0].children[0];

			mouseoverFun(element);
		});
	};

	var cntr = $("#chart"),
	Cw = cntr.width(),
	Ch = cntr.height();

	// Fade all but the current sequence, and show it in the breadcrumb trail.
	function mouseoverFun(d) {
		var desc;
		if (d.depth == 2) {
			desc = d.parent.name + ' ' + d.name;
		} else {
			desc = d.name
		};
		var percentage = (100 * d.value / totalSize).toPrecision(3);
		var percentageString = percentage + "%";
		if (percentage < 0.1) {
			percentageString = "< 0.1%";
		}

		d3.select("#percentage")
		.text(percentageString);

		d3.select("#desc")
		.text(desc);

		d3.select("#explanation")
		.style("visibility", "");

		var sequenceArray = getAncestors(d);
		updateBreadcrumbs(sequenceArray, percentageString);

		// Fade all the segments.
		vis.selectAll("#burstPath")
		.style("opacity", 0.3);

		// Then highlight only those that are an ancestor of the current segment.
		vis.selectAll("#burstPath")
		.filter(function (node) {
			return (sequenceArray.indexOf(node) >= 0);
		})
		.style("opacity", 1);
	}

	// Restore everything to full opacity when moving off the visualization.
	function mouseleaveFun(d) {

		// Hide the breadcrumb trail
		d3.select("#trail")
		.style("visibility", "hidden");

		// Deactivate all segments during transition.
		d3.selectAll("#burstPath").on("mouseover", null);

		// Transition each segment to full opacity and then reactivate it.
		d3.selectAll("#burstPath")
		.transition()
		.duration(1000)
		.style("opacity", 1)
		.each("end", function () {
			d3.select(this).on("mouseover", mouseoverFun);
		});

		d3.select("#explanation")
		.style("visibility", "hidden");
	}

	// Given a node in a partition layout, return an array of all of its ancestor
	// nodes, highest first, but excluding the root.
	function getAncestors(node) {
		var path = [];
		var current = node;
		while (current.parent) {
			path.unshift(current);
			current = current.parent;
		}
		return path;
	}

	function initializeBreadcrumbTrail() {
		if (d3.select("#sequence").empty()) {
			$("#" + chartContainerID).prepend("<div id='sequence'></div>");
		}
		// Add the svg area.
		var trail = d3.select("#sequence").append("svg:svg")
			.attr("width", width)
			.attr("height", 50)
			.attr("id", "trail");
		// Add the label at the end, for the percentage.
		trail.append("svg:text")
		.attr("id", "endlabel")
		.style("fill", "#000");
	}

	// Generate a string that describes the points of a breadcrumb polygon.
	function breadcrumbPoints(d, i) {
		var points = [];
		points.push("0,0");
		points.push(b.w + ",0");
		points.push(b.w + b.t + "," + (b.h / 2));
		points.push(b.w + "," + b.h);
		points.push("0," + b.h);
		if (i > 0) { // Leftmost breadcrumb; don't include 6th vertex.
			points.push(b.t + "," + (b.h / 2));
		}
		return points.join(" ");
	}

	// Update the breadcrumb trail to show the current sequence and percentage.
	function updateBreadcrumbs(nodeArray, percentageString) {

		// Data join; key function combines name and depth (= position in sequence).
		var g = d3.select("#trail")
			.selectAll("g")
			.data(nodeArray, function (d) {
				return d.name + d.depth;
			});

		// Add breadcrumb and label for entering nodes.
		var entering = g.enter().append("svg:g");

		entering.append("svg:polygon")
		.attr("points", breadcrumbPoints)
		.style("fill", function (d, i) {
			if (d.depth == 2) {
				return delaysColors[d.name]
			} else {
				return fill(d.name)
			}
		});

		entering.append("svg:text")
		.attr("x", (b.w + b.t) / 2)
		.attr("y", b.h / 2)
		.attr("dy", "0.35em")
		.attr("text-anchor", "middle")
		.text(function (d) {
			return d.name;
		})
		.classed('white', true);

		// Set position for entering and updating nodes.
		g.attr("transform", function (d, i) {
			return "translate(" + i * (b.w + b.s) + ", 0)";
		});

		// Remove exiting nodes.
		g.exit().remove();

		// Now move and update the percentage at the end.
		d3.select("#trail").select("#endlabel")
		.attr("x", (nodeArray.length + 0.4) * (b.w + b.s))
		.attr("y", b.h / 2)
		.attr("dy", "0.35em")
		.attr("text-anchor", "middle")
		.text(percentageString);

		// Make the breadcrumb trail visible, if it's hidden.
		d3.select("#trail")
		.style("visibility", "");

	}

	function drawLegend() {

		// Dimensions of legend item: width, height, spacing, radius of rounded rect.
		var li = {
			w: width / 7.3,
			h: 25,
			s: 3,
			r: 3
		};

		if (d3.select("#legend").empty()) {
			$("#" + chartContainerID).append("<div id='legend' style='visibility; width:100%;'></div>");
		}
		var legend = d3.select("#legend").append("ul")
						.attr("width", '100%')
						.attr("height", (li.h + li.s))
						.attr("class", "list-inline");

		var keys = legend.selectAll("li.key")
			.data(d3.entries(delaysColors))
			.enter().append("li")
			.style("background", function (d) {
				return d.value;
			})
			.attr("class", "key tooltips")
			.attr("data-original-title", function (d) {
				return delaysDesc[d.key]
			});

		keys.append("text")
		.attr("x", li.w / 2)
		.attr("y", li.h / 2)
		.attr("dy", "0.32em")
		.attr("text-anchor", "middle")
		.text(function (d) {
			return d.key;
		})
		.classed('white', true);
	}

	function toggleLegend() {
		var legend = d3.select("#legend");
		if (legend.style("visibility") !== "hidden") {
			legend.style("visibility", "hidden");
		} else {
			legend.style("visibility", "");
		}
	}

	// Take a 2-column CSV and transform it into a hierarchical structure suitable
	// for a partition layout. The first column is a sequence of step names, from
	// root to leaf, separated by hyphens. The second column is a count of how
	// often that sequence occurred.
	function buildHierarchy(csv) {
		var root = {
			"name": "root",
			"children": []
		};
		for (var i = 0; i < csv.length; i++) {
			var sequence = csv[i][0];
			var size = +csv[i][1];
			if (isNaN(size)) { // e.g. if this is a header row
				continue;
			}
			var parts = sequence.split("-");
			var currentNode = root;
			for (var j = 0; j < parts.length; j++) {
				var children = currentNode["children"];
				var nodeName = parts[j];
				var childNode;
				if (j + 1 < parts.length) {
					// Not yet at the end of the sequence; move down the tree.
					var foundChild = false;
					for (var k = 0; k < children.length; k++) {
						if (children[k]["name"] == nodeName) {
							childNode = children[k];
							foundChild = true;
							break;
						}
					}
					// If we don't already have a child node for this branch, create it.
					if (!foundChild) {
						childNode = {
							"name": nodeName,
							"children": []
						};
						children.push(childNode);
					}
					currentNode = childNode;
				} else {
					// Reached the end of the sequence; create a leaf node.
					childNode = {
						"name": nodeName,
						"size": size
					};
					children.push(childNode);
				}
			}
		}
		return root;
	};
}
