function tryRegisterCheckboxListWithSearch() {
	if (!window.YUI) {
		window.setTimeout(tryRegisterCheckboxListWithSearch,500);
		return;
	}	
	registerCheckboxListWithSearch();
}
tryRegisterCheckboxListWithSearch();

function registerCheckboxListWithSearch() {
	YUI.add('checkboxListWithSearch', function (Y) {
		var Lang = Y.Lang,
			TRIGGER = 'checkbox-list-with-search';

		if (LogiXML.Ajax.AjaxTarget) {
			LogiXML.Ajax.AjaxTarget().on('reinitialize', function () { Y.LogiXML.checkboxListWithSearch.createElements(true); });
		}

		Y.LogiXML.Node.destroyClassKeys.push(TRIGGER);

		Y.namespace('LogiXML').checkboxListWithSearch = Y.Base.create('checkboxListWithSearch', Y.Base, [], {
			_handlers: {},

			configNode: null,
			id: null,
			inpTextBox: null,

			initializer: function(config) {
				this._parseHTMLConfig();
				this.configNode.setData(TRIGGER, this);
				this.initSearch();
			},
			
			initSearch: function() {
			   this.inpTextBoxContainer = Y.Node.create('<div id="' + this.id + '_search_container" class="checkbox-list-search-container"></div>');
               this.configNode.insert(this.inpTextBoxContainer,0);
			   this.inpTextBox = Y.Node.create('<input type="text" name="' + this.id + '_search" id="' + this.id + '_search" class="checkbox-list-search" />');
			   this.inpTextBoxContainer.append(this.inpTextBox);
			   var self = this;
			   /*this._handlers.onsearch = this.inpTextBox.on('keypress', function () {
				   setTimeout(function() {
					   self.applySearch()}, 
					   100);
				}, this);
				*/
				this._handlers.onsearch = this.inpTextBox.on('keyup', function () {
				   setTimeout(function() {
					   self.applySearch()}, 
					   100);
				}, this);
				
				var checkboxDropDown = Y.one('#' + this.id.replace('rdICL-','') + '_handler');
				this._handlers.onclick = checkboxDropDown.on('click', function () {
				  
				   setTimeout(function() { self.inpTextBox.focus(); self.inpTextBox.set('value', '');self.applySearch();}, 100);
				}, this);
			},
			
			applySearch: function () {
			   var searchText = this.inpTextBox.get('value');
			   console.log(searchText);
				   
				var allItems = this.configNode.all('li');
				if(!searchText || searchText.length == 0) {
					Y.each(allItems, function (item) {
						item.show();
					}, this);
					return;
				}					
				var rx = new RegExp(searchText, 'i');
				Y.each(allItems, function (item) {
					if(rx.test(item._node.innerText)) {
						item.show();
					} else {
						item.hide();
					}
				}, this);
				
				var check_all = this.configNode.one('#' + this.id.replace('rdICL-','') + '_check_all');
				check_all.get('parentNode').get('parentNode').hide();
			},
			
			destructor: function () {
				var configNode = this.configNode;
				this._clearHandlers();
				configNode.setData(TRIGGER, null);
			},

			_clearHandlers: function() {
				var self = this;
				Y.each(this._handlers, function(item) {
					if (item) {
						item.detach();
						item = null;
					}
				});

				Y.each(this.refreshTimers, function (item) {
					if (item) {
						clearTimeout(item);
					}
				});
			},

			_parseHTMLConfig: function() {

				this.configNode = this.get('configNode');
				this.id = this.configNode.getAttribute('id');
			}
		}, {
			// Static Methods and properties
			NAME: 'checkboxListWithSearch',
			ATTRS: {
				configNode: {
					value: null,
					setter: Y.one
				}
			},

			createElements: function (isAjax) {
				if (!isAjax) {
					isAjax = false;
				}

				var search;

				Y.all('.' + TRIGGER).each(function (node) {
					search = node.getData(TRIGGER);
					if (!search) {
						search = new Y.LogiXML.checkboxListWithSearch({
							configNode: node,
							isAjax: isAjax
						});
					}
				});
			}
		});

	}, '1.0.0', { requires: ['base', 'node', 'event', 'node-custom-destroy', 'json-parse', 'io-xdr'] });
	YUI().use('checkboxListWithSearch', function (Y) {
        Y.LogiXML.checkboxListWithSearch.createElements(false);
    });
}

