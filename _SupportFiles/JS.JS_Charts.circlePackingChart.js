drawCirclePacking ('circlePackingChart', circlePackingJson);

function drawCirclePacking (chartContainerID, dataJson){
	var array_1 = [];//basic array
	var array_2 = [];//second array
	var array_3 = [];
	function removeDuplicates(data, matchItem) {
		// removes duplicated items in an array
		var arr = {};
		for (var i = 0; i < data.length; i++)
			arr[data[i][matchItem]] = data[i];
		data = new Array();
		for (var key in arr)
			data.push(arr[key]);
		return data;
	}

	for(var i=0;i<dataJson.length;i++){
		var item = dataJson[i];
		array_1.push({
			"name":item[0],	
			"id":item[1],
			"state":item[2],
			"city":item[3],
			"value":item[4]
			
		})
	}
	for(var i=0;i<dataJson.length;i++){
		var item2 = dataJson[i];
		array_2.push(
			{
				"name":item2[2],	
				"children" : [] 
			})
	}
	array_2 = removeDuplicates(array_2,"name");

	for(var i=0; i<dataJson.length; i++){
		var item_3 = dataJson[i];
		for(var j=0; j<array_2.length; j++){
			if(item_3[2] == array_2[j].name  ){
				array_2[j].children.push({
					"name":item_3[3],
					"children":[]
				})
			}
		}			
	}

	for(i=0;i<array_2.length;i++)
	array_2[i].children = removeDuplicates(array_2[i].children,"name");
			
	for(var i = 0;i<dataJson.length;i++){
		var item_4 = dataJson[i];
		for(var j=0; j<array_2.length;j++){
			for(h=0;h<array_2[j].children.length;h++){
				if(item_4[3] == array_2[j].children[h].name){
					array_2[j].children[h].children.push(
					{
						"name":item_4[0],
						"id":item_4[1],   
						"size":item_4[4]
					})
				}
			}
		}
	}
	var rootObj1 = {
		"name" : "jdiwi",
		"children" : array_2
	}

	var margin = 20, diameter = 740;
	//var format_num = d3.format("s");
	var format_num = d3.format(",.2r");
	function colores_google(n) {
		var colores_g = ["#3366cc", "#dc3912", "#ff9900", "#109618", "#990099", "#0099c6", "#dd4477", "#66aa00", "#b82e2e", "#316395", "#994499", "#22aa99", "#aaaa11", "#6633cc", "#e67300", "#8b0707", "#651067", "#329262", "#5574a6", "#3b3eac"];
		return colores_g[n % colores_g.length];
	}
	var linearScale = d3.scale.linear()
					.domain([3,29202])
					.range(["#BBDEFB","#0D47A1"]);
	
	var color = d3.scale.ordinal()
		  .domain([0,1,2,3,4])
		  .range(['#f2f2f2','#E5E5E5','#d3d3d3','#5f9ea0','#cd5c5c']);

	var pack = d3.layout.pack()
		// .sort( function(a, b) {return 1;})
		.padding(75)
		.size([diameter - margin, diameter - margin])
		.value(function(d) { return d.size; })

	var svg = d3.select("#"+chartContainerID)
		.append("div")
		.classed("svg-container", true)
		//container class to make it responsive
		.append("svg")
		.attr("preserveAspectRatio", "xMinYMin meet")
		.attr("viewBox", "0 0 950 800")
		//.attr("id","svg1")
		//.style("padding-left","9%")
		.append("g")
		.attr("transform", "translate(" + diameter / 1.56 + "," + diameter / 1.82 + ")");
		
	var root = rootObj1;

	var focus = root,
	  nodes = pack.nodes(root),
	  view;

	var circle = svg.selectAll("circle")
	  .data(nodes)
	  .enter().append("circle")
	  .attr("class", function(d) { return d.parent ? d.children ? "node" : "node node--leaf" : "node node--root"; })
	  .style("fill", function(d,i) { return d.children ? color(d.depth) : linearScale(d.value) ; })
	  .on("click", function(d) { if (focus !== d) zoom(d), d3.event.stopPropagation(); });


	var text = svg.selectAll("text")
	  .data(nodes)
	  .enter().append("text")
	  //.attr("class", "label")
	  .attr("dy", function(d){if (d.parent ===root){return d.r/-0.99;}})
	  // change the city font size 
	  .style("font-size",function(d) { return d.parent === root  ? "1em" :(d.depth==2 ? ".8em":"1em" ); })
	  .attr("class", function(d){return (d.parent===root ? "label" : "label text-shadow")})
	  .style("fill",function(d){return (d.parent===root ? "rgb(93, 138, 153)" : "#ffffff")})
	  .style("fill-opacity", function(d) { return (d.parent === root || (d.depth == 2 && d !== focus && d.r > 35)) ? 1 : 0;})
	  .style("display", function(d) { return ((d.parent === root) || (d.depth == 2 && d !== focus && d.r > 35)||(d.depth==2) || (d.depth==3)) ?  "inline" : "none"; });
	  //.text(function(d) { return d.insertLinebreaks ;});
	  
	var node = svg.selectAll("circle,text");

	d3.select("#"+chartContainerID)
	   .on("click", function() { zoom(root); });

	zoomTo([root.x, root.y, root.r * 2 + margin]);
	var bVar = false;
	function zoom(d) {
		var focus0 = focus; focus = d;
		
		var transition = d3.transition()
			.duration(d3.event.altKey ? 7500 :950)
			.tween("zoom", function(d) {
				var i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2 + margin]);
				return function(t) { zoomTo(i(t)); };
			});
		
		transition.selectAll("text")
			.filter(function (d) { return d.parent === focus || this.style.display === "inline"; })
			.style("fill-opacity", function(d) { return (d.parent === focus ||(d.depth == 2 && d !== focus && d.r>35)) ? 1 : 0; })
			.each("start", insertLinebreaks )
			.each("end"  , insertLinebreaks);
			//.each(insertLinebreaks);				
	}

	function zoomTo(v) {
	var k = diameter / v[2]; view = v;
	node.attr("transform", function(d) { return "translate(" + (d.x - v[0]) * k + "," + (d.y - v[1]) * k + ")"; });
	circle.attr("r", function(d) { return d.r * k; });
	}

	var insertLinebreaks = function (d) {
		var el1;
		var el = d3.select(this);
		el1 = el;
		el.selectAll("*").remove();
		
		var words = [];
		if(d.parent == root){
			words.push(d.name.toString()+': '+format_num(d.value).toString());
		}
		else if(d.children == undefined){
			words.push(d.parent.name.toString());
			words.push(d.name.toString());
			words.push(format_num(d.value).toString());
		}
		else if(d.parent != undefined && d.parent.depth == 1 && d.parent !== focus){
		   words.push(d.name.toString());
			
		}else{
			words.push(d.name.toString());
			words.push(format_num(d.value).toString());
			el.text('');
		}
		
		for (var i = 0; i < words.length; i++) {
			
			var tspan = el1.append('tspan').text(words[i]);
			if (i > 0){
				tspan.attr('x', 0).attr('dy', '35');
			}
			if (words.length == 3 && i == 0){
				tspan.style("font-size","20px");
			}
		}

		return (d.parent === focus ||(d.depth == 2 && d !== focus)) ? 1 : 0;
	};

	var allText = svg.selectAll("text");
	allText.each(insertLinebreaks);

	d3.select(self.frameElement).style("height", diameter + 100 + "px");

	var chart = $("#svg1"),
		aspect = chart.width() / chart.height(),
		container = chart.parent();
		
	$(window).on("resize", function() {
		var targetWidth = container.width();
		chart.attr("width", targetWidth);
		chart.attr("height", Math.round(targetWidth / aspect));
	}).trigger("resize");
}