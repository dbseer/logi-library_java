$(function(){ //DOM Ready

	//hide freeform container
	var freeformDiv = $(".freeformPanelContainer");
	//freeformDiv.hide();
	
	//create gridster container
	var divContainer = $("<div class='gridster' />");
	freeformDiv.after( divContainer )
	var ul = $("<ul />");
	divContainer.append(ul);
	
	//move panels into gridster
	var panels = $(".rdDashboardPanel");
	panels.each(function(i, obj) {
		var pnl = $(obj);
		if (pnl.attr('ID') == 'rdDashboardListTable') 
		{
			return;
		}
		//remove resizers
		Y.one(obj).detachAll();
		
		var li = $("<li />");
		li.attr('ID', 'li_' + pnl.attr('ID'));
		ul.append(li);
		
		var styles = pnl.attr('style').split(';'),
			i= styles.length,
			style={}, k, v, kv;


		while (i--)
		{
			kv = styles[i].split(':');
			k = $.trim(kv[0]);
			v = $.trim(kv[1]);
			if (k.length > 0 && v.length > 0)
			{
				style[k] = v;
			}
		}

		console.log(styles, style);

		if(style['data-col']) {
			li.attr("data-row", style['data-row']);
			li.attr("data-col", style['data-col']);
			li.attr("data-sizex", style['data-sizex']);
			li.attr("data-sizey", style['data-sizey']);
			
		} else {
			li.attr("data-row", "1");
			li.attr("data-col", "1");
			var pnlWidth = 1;
			var wIdx = pnl.attr('ID').indexOf('_w');
			if( wIdx > -1)  {
				pnlWidth = parseInt(pnl.attr('ID').substr(wIdx,3).replace('_w', ''));
			}
			li.attr("data-sizex", pnlWidth);	

			var pnlHeight = 1;
			var hIdx = pnl.attr('ID').indexOf('_h');
			if( hIdx > -1)  {
				pnlHeight = parseInt(pnl.attr('ID').substr(hIdx,3).replace('_h', ''));
			}
			li.attr("data-sizey", pnlHeight);	
		}
		pnl.css('position', '');
		pnl.css('width', '');
		pnl.css('height', '');
		pnl.css('display', 'block');
		
		var pnlBody = $(pnl.find('.panelInnerTable'));
		pnlBody.attr('width', '100%');
		
		//permanent panel
		if(pnl.attr('ID').indexOf('_fixed') > -1)  {
			var pnlID = pnl.attr('ID').split('-')[1];
			var mnuRemove = $(pnl.find('#ppoRemove_'+pnlID+'_rdPopupOptionItem'));
			mnuRemove.remove();
		}
		
	});
	var dashboard = $('span.rdDashboard');
	var dashboardId = dashboard.attr('ID');
	var cols = 3;
	var idx = dashboardId.indexOf('_col');
	if( idx > -1)  {
		var colStr = dashboardId.substr(idx);
		cols = parseInt(colStr.replace('_col', ''));
	}
	
    $(".gridster>ul").gridster({
        widget_margins: [10, 10],
        widget_base_dimensions: ['auto', 400],
		max_cols: cols,
		min_cols: cols,
		draggable : {
			stop: function(e) {
				SavePanelPosition(e);
			}
		}
    }).width("auto");
	
	panels.each(function(i, obj) {
		var pnl = $(obj);
		var li = $('#li_' + pnl.attr('ID'));
		li.append(obj);
	});
	
	
	
	function SavePanelPosition(e) 
	{
		var gridster = $(".gridster>ul").gridster().data('gridster');
		var data = gridster.serialize();
		console.log(data);
		var panels = $("li .rdDashboardPanel");
		panels.each(function(i, obj) {
			var pnl = $(obj);
			var container = $(pnl.parent());
			var id = pnl.attr('id');
			var col = container.attr('data-col');
			var row = container.attr('data-row');
			var sizex = container.attr('data-sizex');
			var sizey = container.attr('data-sizey');
			var width = container.width();
			var height = container.height();
			var offset = container.offset();
			var left = offset.left;
			var top = offset.top;
			console.log(id, col, row, sizex, sizey, width, height, left, top);
		});
		
		
		// Go through each of the panels and save their CSS styling
		var eleHiddenPanelOrder = Y.one('#rdDashboardPanelOrder'),
			dashboardPanels = Y.all('li .rdDashboardPanel'),
			numberofPanels = dashboardPanels.size(),
			i, panel, panelSettings = '';

		for (i = 0; i < numberofPanels; i++) {
			panel = dashboardPanels.item(i);
			panelSettings += ',' + Y.LogiInfo.Dashboard.prototype.rdGetPanelInstanceId(panel.getDOMNode());
			panelSettings += ':0:STYLE=';
			// Instead of cssText property, manually grab the styles we want to save
			panelSettings += 'z-index:' + panel.getComputedStyle('zIndex') + ';';
			panelSettings += ' position: ' + panel.getComputedStyle('position') + ';';
			panelSettings += ' left: ' + panel.getComputedStyle('left') + ';';
			panelSettings += ' top: ' + panel.getComputedStyle('top') + ';';
			panelSettings += ' width: ' + panel.getComputedStyle('width') + ';';
			panelSettings += ' height: ' + (parseInt(panel.getComputedStyle('height'), 10)) + 'px' + ';';
			
			var container = panel.get('parentNode');
			panelSettings += ' data-col: ' + container.getAttribute('data-col') + ';';
			panelSettings += ' data-row: ' + container.getAttribute('data-row') + ';';
			panelSettings += ' data-sizex: ' + container.getAttribute('data-sizex') + ';';
			panelSettings += ' data-sizey: ' + container.getAttribute('data-sizey') + ';';
		}


		eleHiddenPanelOrder.set('value', panelSettings);

		var rdPanelParams = "&rdReport=" + document.getElementById("rdDashboardDefinition").value;
		rdPanelParams += "&rdFreeformLayout=True";

		window.status = "Saving dashboard panel positions.";
		rdAjaxRequestWithFormVars('rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=UpdateDashboardPanelOrder' + rdPanelParams);
	}
});

