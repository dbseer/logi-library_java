YUI.add('select2', function (Y) {
		//"use strict";

		var Lang = Y.Lang,
			TRIGGER = 'Select2';
			
		Y.LogiXML.Node.destroyClassKeys.push(TRIGGER);
		
		Y.namespace('LogiXML').Select2 = Y.Base.create('Select2', Y.Base, [], {
			handlers: null,
			configNode: null,
			id: null,
			jquerySelect: null,
	 
			initializer: function (config) {
				var self = this;
				this.handlers = {},
				this._parseHTMLConfig();
				
				this.attachSelect2();
				this.configNode.setData(TRIGGER, this);
			},
			
			attachSelect2: function() {
				var options = {};
				options.width = '100%';
				
				options.minimumResultsForSearch = $(this.configNode._node).hasClass('no-search') ? -1 : 0;
				options.ajax = $(this.configNode._node).hasClass("ajax-search") ? this.getAjaxParams() : null;
				var body = $(this.configNode._node);
				
				
				if(body.hasClass("input-search"))
				{
					options.allowClear = false;
				}
				else 
				{
 					options.allowClear = false;
					options.minimumResultsForSearch= 'Infinity';
				}	
								
				options.closeOnSelect = !$(this.configNode._node).hasClass("checkList");
							
				this.jquerySelect = $(this.configNode._node).select2(options);
				this.jquerySelect.on("select2:select", function (e) { 
					if (!e.params || !e.params.data) {
						return;
					}
					var self = $(this);
					if (e.params.data.id == "-1"){
						//user selected "all" option
						//remove all other options
						self.val("-1").trigger("change"); 
					} else {
						//remove 'all' option if exists
						var selectedIds = self.val();
						if (!selectedIds || !selectedIds.length || selectedIds.length == 0 || selectedIds.indexOf('-1') == -1) {
							return;
						}
						var selectedIdsWithoutAll = [],
							i = 0; length = selectedIds.length;
						for (; i < length; i++) {
							if (selectedIds[i] != '-1') {
								selectedIdsWithoutAll.push(selectedIds[i]);
							}
						}
						self.val(selectedIdsWithoutAll).trigger("change"); 
					}
				});
				// set all option when unselecting the last option
				this.jquerySelect.on("select2:unselect", function (e) { 
					var self = $(this);
					var elSize = self.children('option').length;
					var selectedIds = self.val();
					if((selectedIds == null || selectedIds.length == 0) && elSize > 1){
						self.val("-1").trigger("change");
					}else if((selectedIds == null || selectedIds.length == 0) && elSize == 1){
						var selectedValue = self.children('option')[0].attributes.value.nodeValue;
						self.val(selectedValue).trigger("change");
					}	
				});
			},

			_parseHTMLConfig: function () {
				this.configNode = this.get('configNode');
				this.id = this.configNode.getAttribute('id');
			},
			

			destructor: function () {
				var configNode = this.configNode;

				$(this.configNode._node).select2("destroy"); 

				this._clearHandlers();
				configNode.setData(TRIGGER, null);

				
			},

			_clearHandlers: function () {
				var self = this;
				Y.each(this.handlers, function (item) {
					if (item) {
						if (item.detach) {
							item.detach();
						}
						if (item.destroy) {
							item.destroy();
						}
						item = null;
					}
				});
			}

		}, {
			// Static Methods and properties
			NAME: 'Select2',
			ATTRS: {
				configNode: {
					value: null,
					setter: Y.one
				}
			},

			createElements: function (cssSelector) {

				var element;
				Y.all(cssSelector).each(function (node) {
					element = node.getData(TRIGGER);
					if (!element) {
						element = new Y.LogiXML.Select2({
							configNode: node
						});
					}
				});
			}


		});

	}, '1.0.0', { requires: ['base', 'node', 'event', 'node-custom-destroy', 'json-parse', 'stylesheet', 'event-custom'] });
	
	
Y.use('select2', function(Y) { Y.on('domready', function() {
		var cssSelector = '.applyselect2';
		Y.LogiXML.Select2.createElements(cssSelector);
		if (LogiXML.Ajax.AjaxTarget) {
			LogiXML.Ajax.AjaxTarget().on('reinitialize', function () { Y.LogiXML.Select2.createElements(cssSelector); });
		}
		
});});